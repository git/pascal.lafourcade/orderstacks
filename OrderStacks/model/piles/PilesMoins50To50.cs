﻿using OrderStacks.model.card.cardType;

namespace OrderStacks.model.piles
{
    public class PilesMoins51To51 : Piles
    {
        /**
         * <param name="nbPile">Nombre de pile</param>
         *
         * Constructeur
         */
        public PilesMoins51To51(int nbPile) : base(nbPile)
        {
            for (int i = 0; i < nbPile; i++)
            {
                if (i < (nbPile * 0.5))
                {
                    ListOrderedStacks[i].Push(new ClassicCard(-50m));
                }
                else
                {
                    ListOrderedStacks[i].Push(new ClassicCard(50m));
                }
            }
        }
    }
}
