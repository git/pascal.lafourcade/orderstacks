﻿using OrderStacks.model.card.cardType;

namespace OrderStacks.model.piles
{
    public class PilesMoins5To5 : Piles
    {
        /**
         * <param name="nbPile">Nombre de pile</param>
         *
         * Constructeur
         */
        public PilesMoins5To5(int nbPile) : base(nbPile)
        {
            for (int i = 0; i < nbPile; i++)
            {
                if (i < (nbPile * 0.5))
                {
                    ListOrderedStacks[i].Push(new ClassicCard(-5m));
                }
                else
                {
                    ListOrderedStacks[i].Push(new ClassicCard(5m));
                }
            }
        }
    }
}
