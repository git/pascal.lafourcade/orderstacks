﻿using System;
using OrderStacks.model.card;

namespace OrderStacks.model.@event
{
    public class TopRangeChangedEventArgs : EventArgs
    {

        public Card NewTopRangeCard { get; set; }
        public Card OldTopRangeCard { get; set; }
        public int NumStackChanged { get; set; }

        /**
         * <param name="numStackChanged">Index de la pile changé</param>
         * <param name="newTopRangeCard">Nouvelle carte en haut de la pile</param>
         * <param name="oldTopRangeCard">Ancienne carte en haut de la pile</param>
         * Constructeur
         */
        public TopRangeChangedEventArgs(int numStackChanged, Card newTopRangeCard, Card oldTopRangeCard)
        {
            NewTopRangeCard = newTopRangeCard;
            OldTopRangeCard = oldTopRangeCard;
            NumStackChanged = numStackChanged;
        }
    }
}
