﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;
using OrderStacks.Resx;

namespace OrderStacks.model.gameActions.decimals
{
    public class TerminerSonTourDecimal : TerminerSonTour
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        public TerminerSonTourDecimal(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="CurrentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         *
         * Fonction permettant de terminer son tour
         *
         * <returns>Booléen indiquant si le changement de joueur peut se passer</returns>
         */
        override public bool end(List<Card> CurrentHand, List<Card> CurrentCardPlayed)
        {
            if (CurrentHand.Count == 0 || CurrentCardPlayed.Count >= 2)
            {
                return true;
            }
            else
            {
                ErrorMessage = AppResources.StrCardPlayedLessThanTwo;
                return false;
            }

        }

        /**
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         * <param name="playableCard">Liste des cartes jouables</param>
         *
         * Fonction permettant de chercher les cartes pouvants être jouées et les ajoutes à la liste des cartes jouables
         */
        override protected void tryToFindSoluce(List<Card> playableCard, List<Card> CurrentHand)
        {
            int findDownCard = 0;
            int findUpCard = 0;
            List<Card> hand = new List<Card>();
            List<Card> playableDownCard = new List<Card>();
            List<Card> playableUpCard = new List<Card>();

            CurrentHand.ForEach(card => hand.Add(card));


            hand.ForEach(card =>
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.01m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.001m) == 0)
                    {
                        playableDownCard.Add(card);
                    }
                    else if (card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.01m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.001m) == 0)
                    {
                        playableUpCard.Add(card);
                    }
                }
            });

            playableDownCard.ForEach(card =>
            {
                hand.Remove(card);
            });

            playableUpCard.ForEach(card =>
            {
                hand.Remove(card);
            });


            while ((playableDownCard.Count > findDownCard || playableUpCard.Count > findUpCard) && hand.Count > 0)
            {
                findDownCard = playableDownCard.Count;
                findUpCard = playableUpCard.Count;

                hand.ForEach(card =>
                {
                    for (int i = 0; i < playableDownCard.Count; i++)
                    {
                        if (card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.01m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.001m) == 0)
                        {
                            playableDownCard.Add(card);
                        }
                    }
                    for (int i = 0; i < playableUpCard.Count; i++)
                    {
                        if (card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.1m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.01m) == 0 || card.Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.001m) == 0)
                        {
                            playableUpCard.Add(card);
                        }
                    }
                });

                for (int i = findDownCard; i < playableDownCard.Count; i++)
                {
                    hand.Remove(playableDownCard[i]);
                }

                for (int i = findUpCard; i < playableUpCard.Count; i++)
                {
                    hand.Remove(playableUpCard[i]);
                }
            }

            playableDownCard.ForEach(card => playableCard.Add(card));
            playableUpCard.ForEach(card => playableCard.Add(card));

            bool played = false;
            hand.ForEach(card =>
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (i < (ListOrderedStacks.Size * 0.5))
                    {
                        if (card.Value > ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            playableCard.Add(card);
                            played = true;
                            break;
                        }
                    }
                    else if (card.Value < ListOrderedStacks.getStack(i).Peek().Value)
                    {
                        playableCard.Add(card);
                        played = true;
                        break;
                    }
                }

                if (!played)
                {
                    for (int i = 0; i < playableDownCard.Count; i++)
                    {
                        if (card.Value > playableDownCard[i].Value)
                        {
                            playableCard.Add(card);
                            played = true;
                            break;
                        }
                    }
                }

                if (!played)
                {
                    for (int i = 0; i < playableUpCard.Count; i++)
                    {
                        if (card.Value < playableUpCard[i].Value)
                        {
                            playableCard.Add(card);
                            played = true;
                            break;
                        }
                    }
                }
                played = false;
            });
        }

        /**
         * <param name="playableCard">Liste des cartes jouables</param>
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         *
         * Fonction vérifiant que les règles de fin de jeu ne sont pas arrivé
         *
         * <returns>Booléen de possibilité de jeu</returns>
         */
        override protected bool testEndGame(List<Card> playableCard, List<Card> CurrentHand)
        {
            if (CurrentHand.Count == 1)
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (i < ListOrderedStacks.Size * 0.5)
                    {
                        if (CurrentHand[0].Value > ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (CurrentHand[0].Value < ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            return true;
                        }
                    }
                    if (CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 1m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.1m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.01m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value + 0.001m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 1m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.1m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.01m) == 0 || CurrentHand[0].Value.CompareTo(ListOrderedStacks.getStack(i).Peek().Value - 0.001m) == 0)
                    {
                        return true;
                    }
                }
            }
            if (playableCard.Count < 2)
            {
                return false;
            }
            return true;
        }
    }
}
