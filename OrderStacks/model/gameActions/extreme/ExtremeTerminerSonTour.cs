﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.extreme
{
    public class ExtremeTerminerSonTour : TerminerSonTour
    {
        public ExtremeTerminerSonTour(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        public override bool end(List<Card> CurrentHand, List<Card> CurrentCardPlayed)
        {
            throw new NotImplementedException();
        }

        protected override bool testEndGame(List<Card> playableCard, List<Card> CurrentHand)
        {
            throw new NotImplementedException();
        }

        protected override void tryToFindSoluce(List<Card> playableCard, List<Card> CurrentHand)
        {
            throw new NotImplementedException();
        }
    }
}
