﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using OrderStacks.model.card;
using OrderStacks.model.@event;
using OrderStacks.model.piles;
using OrderStacks.Resx;

namespace OrderStacks.model.manager
{
    public abstract class GameManager: INotifyPropertyChanged
    {

        protected Parametreur parametreur;
        protected List<Card> CurrentCardPlayed = new List<Card>();
        public string EndMessage { get; set; } = "";
        protected int currentIndexPlayer;
        protected List<Card> currentHand;
        protected bool win;
        public List<Card> CurrentHand
        {
            get { return currentHand; }
            set
            {
                currentHand = value;
                OnPropertyChanged("CurrentHand");
            }
        }

        #region event

        public event EventHandler<EventArgs> EndGame;
        public event EventHandler<PlayerChangedEventArgs> PlayerChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        /**
         * <param name="info">Nom de la propriété qui change</param>
         * Evénement permettant de notifier qu'une propriété a changé
         */
        public virtual void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        /**
         * <param name="args">Argument(s) de l'événement</param>
         * <param name="source">Source de l'événement</param>
         * Evenement permettant de notifier la fin du jeu
         */
        public void OnEndGame(object source, EventArgs args)
        {
            EndGame?.Invoke(source, args);
        }

        /**
         * <param name="args">Argument(s) de l'événement</param>
         * <param name="source">Source de l'événement</param>
         * Evenement permettant de changer de joueur actif
         */
        protected internal void OnPlayerChanged(object source, PlayerChangedEventArgs args)
        {
            currentIndexPlayer += 1;
            if (currentIndexPlayer == parametreur.players.Count)
            {
                currentIndexPlayer = 0;
            }
            CurrentHand = parametreur.players[currentIndexPlayer].getCardList();
            int i = 0;
            while (CurrentHand.Count == 0 && i != 5)
            {
                currentIndexPlayer += 1;
                if (currentIndexPlayer == parametreur.players.Count)
                {
                    currentIndexPlayer = 0;
                }
                CurrentHand = parametreur.players[currentIndexPlayer].getCardList();
                i++; // Garde fou
            }
            if (i == 5)
            {
                EndMessage = AppResources.StrWin;
                win = true;
            }
            else
            {
                parametreur.GameMode.TestEndGame(CurrentHand);
                PlayerChanged?.Invoke(source, new PlayerChangedEventArgs(CurrentHand, parametreur.players[currentIndexPlayer].Pseudo));
                parametreur.GameMode.NbCardAtBeginOfTurn = CurrentHand.Count;
            }
        }

        #endregion


        /**
         * <param name="parametreur">Parametreur de la partie</param>
         * Constructeur
         */
        protected GameManager(Parametreur parametreur)
        {
            this.parametreur = parametreur;

            parametreur.Prepare();

            parametreur.GameMode.EndGame += OnEndGame;

            parametreur.GameMode.PlayerChanged += OnPlayerChanged;

            CurrentHand = parametreur.players[0].getCardList();
        }

        
        /**
         * <param name="orderedStackSelected">Pile sélectionnée pour essayer de poser la carte</param>
         *<param name="valueCard">Valeur de la carte séléctionnée</param>
         * Fonction permettant de tenter de jouer une carte
         * <return>Booléen de carte joué</return>
         */
        public bool joue(int orderedStackSelected, decimal valueCard)
        {
            bool isPlayed = parametreur.GameMode.playCard(valueCard, currentHand, orderedStackSelected, parametreur.players[currentIndexPlayer], CurrentCardPlayed); // Mettre le joueur actif
            EndMessage = parametreur.GameMode.Message;
            return isPlayed;
        }

        /**
         * Fonction permettant de terminer son tour et notifier si le jeu est fini
         * <returns>Booléen de fin de jeu</returns>
         */
        public bool endTurn()
        {
            if(parametreur.GameMode.endTurn(currentHand, CurrentCardPlayed, parametreur.players[currentIndexPlayer]))
            {
                EndMessage = parametreur.GameMode.Message + "Il vous restait " + parametreur.getScore() + " cartes à jouer!";
                return true;
            }
            else
            {
                EndMessage = parametreur.GameMode.Message;
                if (win)
                {
                    EndMessage = AppResources.StrWin;
                    OnEndGame(this, new EventArgs());
                    return true;
                }
                return false;
            }
        }

        /**
         * Fonction permettant de retourner les piles
         * <returns>Piles du jeu</returns>
         */
        public Piles getPiles()
        {
            return parametreur.GameMode.Piles;
        }
    }
}
