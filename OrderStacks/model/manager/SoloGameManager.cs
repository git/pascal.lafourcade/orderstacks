namespace OrderStacks.model.manager
{
    public class SoloGameManager : GameManager
    {
        /**
         * <param name="parametreur">Parametreur de la partie</param>
         * Constructeur
         */
        public SoloGameManager(Parametreur parametreur)
            : base(parametreur)
        {

        }
    }
}
