﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrderStacks.Resx {
    using System;
    using System.Reflection;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppResources {
        
        private static System.Resources.ResourceManager resourceMan;
        
        private static System.Globalization.CultureInfo resourceCulture;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppResources() {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public static System.Resources.ResourceManager ResourceManager {
            get {
                if (object.Equals(null, resourceMan)) {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("OrderStacks.Resx.AppResources", typeof(AppResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public static System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        public static string StrAbout {
            get {
                return ResourceManager.GetString("StrAbout", resourceCulture);
            }
        }
        
        public static string StrAcendingStack {
            get {
                return ResourceManager.GetString("StrAcendingStack", resourceCulture);
            }
        }
        
        public static string StrBtnPlay {
            get {
                return ResourceManager.GetString("StrBtnPlay", resourceCulture);
            }
        }
        
        public static string StrCantGetStack {
            get {
                return ResourceManager.GetString("StrCantGetStack", resourceCulture);
            }
        }
        
        public static string StrCardDoesntExist {
            get {
                return ResourceManager.GetString("StrCardDoesntExist", resourceCulture);
            }
        }
        
        public static string StrCardPlayedLessThanTwo {
            get {
                return ResourceManager.GetString("StrCardPlayedLessThanTwo", resourceCulture);
            }
        }
        
        public static string StrCloseWind {
            get {
                return ResourceManager.GetString("StrCloseWind", resourceCulture);
            }
        }
        
        public static string StrDownStack {
            get {
                return ResourceManager.GetString("StrDownStack", resourceCulture);
            }
        }
        
        public static string StrEndTurn {
            get {
                return ResourceManager.GetString("StrEndTurn", resourceCulture);
            }
        }
        
        public static string StrEnterPseudo {
            get {
                return ResourceManager.GetString("StrEnterPseudo", resourceCulture);
            }
        }
        
        public static string StrInfo {
            get {
                return ResourceManager.GetString("StrInfo", resourceCulture);
            }
        }
        
        public static string String3 {
            get {
                return ResourceManager.GetString("String3", resourceCulture);
            }
        }
        
        public static string String4 {
            get {
                return ResourceManager.GetString("String4", resourceCulture);
            }
        }
        
        public static string StrLocalGame {
            get {
                return ResourceManager.GetString("StrLocalGame", resourceCulture);
            }
        }
        
        public static string StrLose {
            get {
                return ResourceManager.GetString("StrLose", resourceCulture);
            }
        }
        
        public static string StrMultiplayer {
            get {
                return ResourceManager.GetString("StrMultiplayer", resourceCulture);
            }
        }
        
        public static string StrNbCards {
            get {
                return ResourceManager.GetString("StrNbCards", resourceCulture);
            }
        }
        
        public static string StrNbPlayers {
            get {
                return ResourceManager.GetString("StrNbPlayers", resourceCulture);
            }
        }
        
        public static string StrNbStacksInGame {
            get {
                return ResourceManager.GetString("StrNbStacksInGame", resourceCulture);
            }
        }
        
        public static string StrNext {
            get {
                return ResourceManager.GetString("StrNext", resourceCulture);
            }
        }
        
        public static string StrPlayerSelections {
            get {
                return ResourceManager.GetString("StrPlayerSelections", resourceCulture);
            }
        }
        
        public static string StrPseudoGM {
            get {
                return ResourceManager.GetString("StrPseudoGM", resourceCulture);
            }
        }
        
        public static string StrRetry {
            get {
                return ResourceManager.GetString("StrRetry", resourceCulture);
            }
        }
        
        public static string StrTiret1 {
            get {
                return ResourceManager.GetString("StrTiret1", resourceCulture);
            }
        }
        
        public static string StrTiret10 {
            get {
                return ResourceManager.GetString("StrTiret10", resourceCulture);
            }
        }
        
        public static string StrTiret11 {
            get {
                return ResourceManager.GetString("StrTiret11", resourceCulture);
            }
        }
        
        public static string StrTiret12 {
            get {
                return ResourceManager.GetString("StrTiret12", resourceCulture);
            }
        }
        
        public static string StrTiret13 {
            get {
                return ResourceManager.GetString("StrTiret13", resourceCulture);
            }
        }
        
        public static string StrTiret2 {
            get {
                return ResourceManager.GetString("StrTiret2", resourceCulture);
            }
        }
        
        public static string StrTiret3 {
            get {
                return ResourceManager.GetString("StrTiret3", resourceCulture);
            }
        }
        
        public static string StrTiret4 {
            get {
                return ResourceManager.GetString("StrTiret4", resourceCulture);
            }
        }
        
        public static string StrTiret5 {
            get {
                return ResourceManager.GetString("StrTiret5", resourceCulture);
            }
        }
        
        public static string StrTiret6 {
            get {
                return ResourceManager.GetString("StrTiret6", resourceCulture);
            }
        }
        
        public static string StrTiret7 {
            get {
                return ResourceManager.GetString("StrTiret7", resourceCulture);
            }
        }
        
        public static string StrTiret8 {
            get {
                return ResourceManager.GetString("StrTiret8", resourceCulture);
            }
        }
        
        public static string StrTiret9 {
            get {
                return ResourceManager.GetString("StrTiret9", resourceCulture);
            }
        }
        
        public static string StrTitle {
            get {
                return ResourceManager.GetString("StrTitle", resourceCulture);
            }
        }
        
        public static string StrTitleRules {
            get {
                return ResourceManager.GetString("StrTitleRules", resourceCulture);
            }
        }
        
        public static string StrValueType {
            get {
                return ResourceManager.GetString("StrValueType", resourceCulture);
            }
        }
        
        public static string StrWin {
            get {
                return ResourceManager.GetString("StrWin", resourceCulture);
            }
        }
        
        public static string StrWrongStack {
            get {
                return ResourceManager.GetString("StrWrongStack", resourceCulture);
            }
        }
        
        public static string TypeValuefractionated {
            get {
                return ResourceManager.GetString("TypeValuefractionated", resourceCulture);
            }
        }
        
        public static string TypeValueHundredthFract {
            get {
                return ResourceManager.GetString("TypeValueHundredthFract", resourceCulture);
            }
        }
        
        public static string TypeValueRelative {
            get {
                return ResourceManager.GetString("TypeValueRelative", resourceCulture);
            }
        }
        
        public static string TypeValueTenthFract {
            get {
                return ResourceManager.GetString("TypeValueTenthFract", resourceCulture);
            }
        }
        
        public static string TypeValueThousandthFract {
            get {
                return ResourceManager.GetString("TypeValueThousandthFract", resourceCulture);
            }
        }
        
        public static string TypeValueWhole {
            get {
                return ResourceManager.GetString("TypeValueWhole", resourceCulture);
            }
        }
        
        public static string TypeValueTenth {
            get {
                return ResourceManager.GetString("TypeValueTenth", resourceCulture);
            }
        }
        
        public static string TypeValueHundredth {
            get {
                return ResourceManager.GetString("TypeValueHundredth", resourceCulture);
            }
        }
        
        public static string WhiteTheme {
            get {
                return ResourceManager.GetString("WhiteTheme", resourceCulture);
            }
        }
        
        public static string WrongPseudo {
            get {
                return ResourceManager.GetString("WrongPseudo", resourceCulture);
            }
        }
        
        public static string StrFracValue {
            get {
                return ResourceManager.GetString("StrFracValue", resourceCulture);
            }
        }
        
        public static string StrSpecRules {
            get {
                return ResourceManager.GetString("StrSpecRules", resourceCulture);
            }
        }
        
        public static string StrTiret14 {
            get {
                return ResourceManager.GetString("StrTiret14", resourceCulture);
            }
        }
        
        public static string StrTiret15 {
            get {
                return ResourceManager.GetString("StrTiret15", resourceCulture);
            }
        }
        
        public static string StrTiret16 {
            get {
                return ResourceManager.GetString("StrTiret16", resourceCulture);
            }
        }
        
        public static string StrTiret17 {
            get {
                return ResourceManager.GetString("StrTiret17", resourceCulture);
            }
        }
        
        public static string StrTiret18 {
            get {
                return ResourceManager.GetString("StrTiret18", resourceCulture);
            }
        }
        
        public static string StrTiret19 {
            get {
                return ResourceManager.GetString("StrTiret19", resourceCulture);
            }
        }
        
        public static string StrValueFracDec {
            get {
                return ResourceManager.GetString("StrValueFracDec", resourceCulture);
            }
        }
        
        public static string StrBtnPrevious {
            get {
                return ResourceManager.GetString("StrBtnPrevious", resourceCulture);
            }
        }
    }
}
