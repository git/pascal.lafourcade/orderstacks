﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace OrderStacks.IO
{
    class IOOptions
    {
        static string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Options.xml");

        public static void SaveOptionParameter(bool b)
        {
            
            XmlSerializer ser = new XmlSerializer(typeof(bool));
            TextWriter ws = new StreamWriter(path);
            ser.Serialize(ws, b);
            ws.Close();
        }

        public static bool LoadOptionsParameter()
        {
            XmlSerializer xs = new XmlSerializer(typeof(bool));
            
            try
            {
                using(FileStream fs = new FileStream(path, FileMode.Open))
                {
                    bool b = (bool)xs.Deserialize(fs);
                    return b;                
                }
            }
            catch
            {
                //fs.Close();
                return false;
            }

            
        }

    }
}
