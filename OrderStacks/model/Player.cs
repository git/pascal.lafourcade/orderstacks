﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.@event;

namespace OrderStacks.model
{
    public class Player
    {

        public string Pseudo { get; set; }
        private List<Card> cardList = new List<Card>();
        public event EventHandler<HandCardChangedEventArgs> HandCardChanged;

        /**
         *<param name="pseudo">Pseudo du joueur</param>
         *
         * Constructeur
         */
        public Player(string pseudo)
        {
            Pseudo = pseudo;
        }

        /**
         * <param name="card">Carte pioché</param>
         *
         * Fonction permettant d'ajouter une carte pioché à la main de l'utilisateur
         */
        public void pioche(Card card)
        {
            cardList.Add(card);
            int position = cardList.IndexOf(card);
            OnCardDrawable(new HandCardChangedEventArgs(card, position));
        }

        /**
         * <param name="card">Carte joué</param>
         *
         * Fonction permettant de jouer une carte de la main du joueur
         */
        public void joue(Card card)
        {
            int position = cardList.IndexOf(card);
            cardList.Remove(card);
            OnCardDrawable(new HandCardChangedEventArgs(null, position));
        }

        //public IReadOnlyList<Card> getCardList()
        //{
        //     return cardList.AsReadOnly();
        //}

        /**
         * Fonction permettant de retourner la liste de carte du joueur
         *
         * <returns>Liste de cartes</returns>
         */
        public List<Card> getCardList()
        {
            return cardList;
        }

        /**
         * <param name="args">Argument de l'événement</param>
         *
         * Evénement permettant de notifier que la main a changé
         */
        protected internal void OnCardDrawable(HandCardChangedEventArgs args)
        {
            HandCardChanged?.Invoke(this, args);
        }

    }
}
