﻿using System;

namespace OrderStacks.model.card.rapidCard
{
    public abstract class RapidCard : Card
    {
        /**
         * <param name="value">Valeur de la carte</param>
         * Constructeur
         */
        public RapidCard(int value) : base(value)
        {
        }

        public override bool rapidEffect()
        {
            return true;
        }

    }
}
