﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.deck;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.extreme
{
    public class ExtremeMode : GameMode
    {
        public ExtremeMode(Piles piles, Deck deck) : base(piles, deck)
        {
            gameActions.Add(new ExtremePiocher(Piles));
            gameActions.Add(new ExtremeJouerUneCarte(Piles));
            gameActions.Add(new ExtremeTerminerSonTour(Piles));
        }

        public override bool endTurn(List<Card> currentHand, List<Card> CurrentCardPlayed, Player player)
        {
            throw new NotImplementedException();
        }

        public override void load(int nbPlayer, List<Player> players)
        {
            throw new NotImplementedException();
        }

        public override void pioche(List<Card> currentHand, Player player)
        {
            throw new NotImplementedException();
        }

        public override bool playCard(decimal valueCard, List<Card> currentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed)
        {
            throw new NotImplementedException();
        }

        public override void TestEndGame(List<Card> currentHand)
        {
            throw new NotImplementedException();
        }
    }
}
