﻿using System;
using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.deck;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.classic
{
    public class PiocherClassic : Piocher
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        public PiocherClassic(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="CurrentHand">Liste de carte du joeuur actif</param>
         * <param name="deck">Liste de carte non découverte</param>
         * <param name="nbMaxCard">Nombre maximum de carte dans la main pour le joueur actif</param>
         *
         * Fonction permettant de faire piocher un joueur
         */
        override public void pioche(List<Card> CurrentHand, Deck deck, Player player, int nbMaxCard)
        {
            int nbPickedCard = nbMaxCard - CurrentHand.Count;
            for (int i = 0; i < nbPickedCard; i++)
            {
                if (deck.size() == 0)
                {
                    return;
                }
                int random = new Random().Next(0, deck.size() - 1);
                player.pioche(deck.getCard(random));
                deck.removeAt(random);
            }
        }
    }
}
