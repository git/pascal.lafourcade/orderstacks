﻿using System.ComponentModel;
using OrderStacks.model.card;

namespace OrderStacks.viewmodel
{
    public class CardVM : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public Card View { get; set; }

        protected decimal value;
        public decimal Value {
            get { return value; }
            set
            {
                this.value = value;
                View.Value = value;
                OnPropertyChanged("Value");
            }
        }

        /**
         * <param name="info">Nom de la propriété</param>
         * 
         * Fonction permettant de notifier qu'une propriété a changé
         */
        protected virtual void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        
        /**
         * <param name="view">Card représenté</param>
         * 
         * Constructeur
         */
        public CardVM(Card view)
        {
            View = view;
            Value = view.Value;
        }

        public override string ToString()
        {
            return View.ToString();
        }
    }
}
