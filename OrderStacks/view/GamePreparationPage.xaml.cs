﻿using System;
using System.Collections.Generic;
using OrderStacks.IO;
using OrderStacks.Resx;
using Xamarin.Forms;

namespace OrderStacks.view
{
    public partial class GamePreparationPage : ContentPage
    {

        public List<string> listGameMode = new List<string> { AppResources.TypeValueWhole, AppResources.TypeValueRelative, AppResources.TypeValueTenthFract, AppResources.TypeValueHundredthFract, AppResources.TypeValueThousandthFract, AppResources.TypeValuefractionated, AppResources.TypeValueTenth, AppResources.TypeValueHundredth };

        /**
         * Constructeur de la page * 
         * Charge les derniers paramètres de l'utilisateur sur le nom, le nombre de joueur, le nombre de pile, le valeur en jeu ainsi que le nombre de cartes *
         * Bind les valeurs des slider sur les labels pour rendre un retour des valeurs choisis à l'utilisateur **/
        public GamePreparationPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            LoadParameterName();
            
            nbPlayersChoose.SetBinding(Label.TextProperty, new Binding("Value", source: PlayerSelecter));
            nbstacks.SetBinding(Label.TextProperty, new Binding("Value", source: SelectNbStack));
            nbCard.SetBinding(Label.TextProperty, new Binding("Value", source: SelectNbCard));

            LoadParameterNbPlayerGamePreparation();

            SelectMode.ItemsSource = listGameMode;
            LoadParameterGameModeValueGamePreparation();

            
            LoadParameterNbStacksGamePreparation();

            
            LoadParameterNbCardsGamePreparation();

        }

        /**
         * Permet de retourner sur la page d'accueil grâce à l'image de la flèche et sauvegarde les choix de l'utilisateur lorsqu'il reviendra sur la page GamePreparationPage *
         **/ 
        private async void Back(object sender, EventArgs e)
        {
            IOGamePreparation.SaveParamaterGamePreparationNbPlayers((int)PlayerSelecter.Value);
            IOGamePreparation.SaveParameterGamePreparationGameModeValue(SelectMode.SelectedIndex);
            IOGamePreparation.SaveParameterGamePreparationNbStacks((int)SelectNbStack.Value);
            IOGamePreparation.SaveParameterGamePreparationNbCards((int)SelectNbCard.Value);
            if ((int)PlayerSelecter.Value == 1)
            {
                IOGamePreparation.SaveParameterGamePreparationName(FirstEntry.Text);
            }
            await Navigation.PopToRootAsync();
        }

        /**
         * Permet de retourner sur la page d'accueil grâce au bouton back sur android et sauvegarde les choix de l'utilisateur lorsqu'il reviendra sur la page GamePreparationPage *
         **/
        protected override bool OnBackButtonPressed()
        {
            IOGamePreparation.SaveParamaterGamePreparationNbPlayers((int)PlayerSelecter.Value);
            IOGamePreparation.SaveParameterGamePreparationGameModeValue(SelectMode.SelectedIndex);
            IOGamePreparation.SaveParameterGamePreparationNbStacks((int)SelectNbStack.Value);
            IOGamePreparation.SaveParameterGamePreparationNbCards((int)SelectNbCard.Value);
            if ((int)PlayerSelecter.Value == 1)
            {
                IOGamePreparation.SaveParameterGamePreparationName(FirstEntry.Text);
            }
            return base.OnBackButtonPressed();
        }


        /**
         * Permet d'aller sur la page de jeu et de commencer une partie avec les valeurs choisis *
         * Sauavegarde les valeurs choisis lors du retour sur la page GamePreparationPage *
         * 
         * Return un message si le pseudo définit par l'utilisateur est incorrecte *
         **/ 
        private async void Play(object sender, EventArgs args)
        {
            List<string> playersNames = new List<string>();
            

            for (int i = 1; i < NameList.Children.Count; i++)
            {
                playersNames.Add(((Entry)NameList.Children[i]).Text);
                if (string.IsNullOrWhiteSpace(playersNames[playersNames.Count - 1]))
                {
                    await DisplayAlert(AppResources.WrongPseudo, AppResources.StrEnterPseudo, AppResources.StrCloseWind);
                    return;
                }
            }
            IOGamePreparation.SaveParamaterGamePreparationNbPlayers((int)PlayerSelecter.Value);
            IOGamePreparation.SaveParameterGamePreparationGameModeValue(SelectMode.SelectedIndex);
            IOGamePreparation.SaveParameterGamePreparationNbStacks((int)SelectNbStack.Value);
            IOGamePreparation.SaveParameterGamePreparationNbCards((int)SelectNbCard.Value);
            IOGamePreparation.SaveParameterGamePreparationName(FirstEntry.Text);
            await Navigation.PushAsync(new MainPage(playersNames, (int)SelectNbStack.Value, (int)SelectMode.SelectedIndex, (int)SelectNbCard.Value));
        }

        private bool canModif = true;

        /** 
         * Charge le nom du dernier joueur si le nombre de joueur choisi est égale à 1 *
         * Si le nombre de joueur choisi est supérieur à 1, alors ouvre un nombre d'entrées égale au à la valeur du slider du nombre de joueur * 
         **/ 
        private void ChangedPseudo(object sender, EventArgs args)
        {
            if (canModif)
            {
                canModif = false;
                PlayerSelecter.Value = Math.Round(PlayerSelecter.Value);

                if ((int)PlayerSelecter.Value == 1)
                {
                    LoadParameterName();
                }
                else
                {
                    FirstEntry.Text = "";
                }

                while (NameList.Children.Count - 1 != (int)PlayerSelecter.Value)
                {
                    if (NameList.Children.Count - 1 < (int)PlayerSelecter.Value)
                    {

                        Entry e = new Entry
                        {
                            Placeholder = "Pseudo",
                            BackgroundColor = (Color)Application.Current.Resources["Gold"],
                            WidthRequest = 200,
                            MinimumWidthRequest = 50,
                            HorizontalOptions = LayoutOptions.Center,
                            MaxLength = 18,
                            IsTextPredictionEnabled = false,
                            TextColor = Color.Black
                        };

                        NameList.Children.Add(e);

                    }
                    else
                    {
                        NameList.Children.RemoveAt(NameList.Children.Count - 1);
                    }

                }
                canModif = true;
            }
        }
        /**
         * Permet de choisir un nombre de pile à travers le slider * 
         * Si la valeur du slider est égale à 5 ou 7 alors le slider prendra la valeur soit de 4 soit de 6 *
         **/ 

        private void ChangedStacks(object sender, EventArgs args)
        {
            if (canModif)
            {
                canModif = false;

                if (SelectNbStack.Value < 5)
                {
                    SelectNbStack.Value = 4;
                }
                else if (SelectNbStack.Value < 7)
                {
                    SelectNbStack.Value = 6;
                }
                else
                {
                    SelectNbStack.Value = 8;
                }

                canModif = true;
            }
        }

        /**
         * Permet de choisir un nombre de carte à travers le slider * 
         * Si la valeur du slider est égale à 50 ou 70 ou 90 alors le slider prendra la valeur soit de 40 soit de 60 soit de 80  *
         **/
        private void ChangedNbCards(object sender, EventArgs args)
        {
            if (canModif)
            {
                canModif = false;

                if (SelectNbCard.Value < 50)
                {
                    SelectNbCard.Value = 40;
                }
                else if (SelectNbCard.Value < 70)
                {
                    SelectNbCard.Value = 60;
                }
                else if (SelectNbCard.Value < 90)
                {
                    SelectNbCard.Value = 80;
                }
                else
                {
                    SelectNbCard.Value = 100;
                }
                canModif = true;
            }
        }

        /**
         * Méthode qui charge les paramètres de l'utilisateur sur le nombre de joueurs qui etait définit la dernière fois *
         * Appel de la méthode LoadParameterGamePreparationNbPlayers de la classe IOGamePreparation *
         **/
        public void LoadParameterNbPlayerGamePreparation()
        {
            int nbJoueurs = IOGamePreparation.LoadParameterGamePreparationNbPlayers();
            PlayerSelecter.Value = nbJoueurs;
        }

        /**
         * Méthode qui charge les paramètres de l'utilisateur sur le mode de jeu qui etait définit la dernière fois *
         * Appel de la méthode LoadParameterGamePreparationGameModeValue de la classe IOGamePreparation *
         **/
        public void LoadParameterGameModeValueGamePreparation()
        {
            int gameModeValue = IOGamePreparation.LoadParameterGamePreparationGameModeValue();
            SelectMode.SelectedIndex = gameModeValue;
        }

        /**
         * Méthode qui charge les paramètres de l'utilisateur sur le nombre de piles qui etait définit la dernière fois *
         * Appel de la méthode LoadParamaterGamePreparationNbStacks de la classe IOGamePreparation *
         **/
        public void LoadParameterNbStacksGamePreparation()
        {
            int nbStackSer = IOGamePreparation.LoadParamaterGamePreparationNbStacks();
            SelectNbStack.Value = nbStackSer;
        }
        /**
         * Méthode qui charge les paramètres de l'utilisateur sur le nombre de cartes qui etait définit la dernière fois *
         * Appel de la méthode LoadParameterGamePreparationNbCards de la classe IOGamePreparation *
         **/
        public void LoadParameterNbCardsGamePreparation()
        {
            int nbCards = IOGamePreparation.LoadParameterGamePreparationNbCards();
            SelectNbCard.Value = nbCards;
        }

        /**
         * Méthode qui charge les paramètres de l'utilisateur sur le dernier pseudo saisie définit la dernière fois *
         * Appel de la méthode LoadNameFromGamePrepararion de la classe IOGamePreparation *
         **/
        public void LoadParameterName()
        {
            string pseudo = IOGamePreparation.LoadNameFromGamePrepararion();
            FirstEntry.Text = pseudo;
        }



    }
}
