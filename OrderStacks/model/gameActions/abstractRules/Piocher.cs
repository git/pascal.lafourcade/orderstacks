﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.deck;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.abstractRules
{
    public abstract class Piocher : GameAction
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        public Piocher(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="CurrentHand">Liste de carte du joeuur actif</param>
         * <param name="deck">Liste de carte non découverte</param>
         * <param name="nbMaxCard">Nombre maximum de carte dans la main pour le joueur actif</param>
         *
         * Fonction permettant de faire piocher un joueur
         */
        public abstract void pioche(List<Card> CurrentHand, Deck deck, Player player, int nbMaxCard);
    }
}
