﻿using SkiaSharp;

namespace OrderStacks.view
{
    public class TouchManipulationInfo
    {
        public SKPoint PreviousPoint { set; get; }

        public SKPoint NewPoint { set; get; }
    }
}
