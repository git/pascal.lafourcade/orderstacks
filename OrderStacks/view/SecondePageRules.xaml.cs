﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OrderStacks.view
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SecondePageRules : ContentPage
    {
        public SecondePageRules()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /**
         * Bouton maison qui permet de se rendre sur la page d'accueil *
         **/
        private async void BackToHome(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomePage());
        }
        /**
         * Bouton jouer qui permet de se rendre sur la page GamePreparationPage *
         **/
        private async void ClickToPlay(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new GamePreparationPage());
        }
        /**
         * Bouton suivant qui permet de se rendre sur la troisième page des règles *
         **/
        private async void nextPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new LastPage_Rules());
        }
        /**
         * Bouton suivant qui permet de se rendre sur la troisième page des règles *
         **/
        private async void previousPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}