﻿using System;
using OrderStacks.model.card.cardType;

namespace OrderStacks.model.deck
{
    public class RelativeDeck : Deck
    {
        /**
         * <param name="borneMin">Valeur minimale du deck</param>
         * <param name="borneMax">Valeur maximale du deck</param>
         * <param name="nbCard">Nombre de carte dans le deck</param>
         * Constructeur
         */
        public RelativeDeck(int nbCard, int borneMin, int borneMax) : base(nbCard)
        {
            Random random = new Random();
            while (deck.Count < nbCard && deck.Count < (borneMax - borneMin))
            {
                int value = random.Next(borneMin, borneMax);
                InsertionDichotomique(0, deck.Count-1, new ClassicCard(value));
            }
        }
    }
}
