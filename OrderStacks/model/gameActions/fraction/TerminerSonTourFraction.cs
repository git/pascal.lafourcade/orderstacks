﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.card.cardType;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;
using OrderStacks.Resx;

namespace OrderStacks.model.gameActions.fraction
{
    public class TerminerSonTourFraction : TerminerSonTour
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        public TerminerSonTourFraction(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="CurrentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         *
         * Fonction permettant de terminer son tour
         *
         * <returns>Booléen indiquant si le changement de joueur peut se passer</returns>
         */
        public override bool end(List<Card> CurrentHand, List<Card> CurrentCardPlayed)
        {
            if (CurrentHand.Count == 0 || CurrentCardPlayed.Count >= 2)
            {
                // Vérifier la fin du jeu
                return true;
            }
            else
            {
                ErrorMessage = AppResources.StrCardPlayedLessThanTwo;
                return false;
            }
        }

        /**
         * <param name="playableCard">Liste des cartes jouables</param>
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         *
         * Fonction vérifiant que les règles de fin de jeu ne sont pas arrivé
         *
         * <returns>Booléen de possibilité de jeu</returns>
         */
        protected override bool testEndGame(List<Card> playableCard, List<Card> CurrentHand)
        {
            if (CurrentHand.Count == 1)
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (i < ListOrderedStacks.Size * 0.5)
                    {
                        if (CurrentHand[0].Value > ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (CurrentHand[0].Value < ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            return true;
                        }
                    }
                    if (((FractionCard)CurrentHand[0]).Fraction.isMultiple(((FractionCard)ListOrderedStacks.getStack(i).Peek()).Fraction))
                    {
                        return true;
                    }
                }
            }
            if (playableCard.Count < 2)
            {
                return false;
            }
            return true;
        }

        /**
         * <param name="CurrentHand">Liste de carte du joueur actif</param>
         * <param name="playableCard">Liste des cartes jouables</param>
         *
         * Fonction permettant de chercher les cartes pouvants être jouées et les ajoutes à la liste des cartes jouables
         */
        protected override void tryToFindSoluce(List<Card> playableCard, List<Card> CurrentHand)
        {
            int findMultipleCard = 0;
            List<Card> hand = new List<Card>();
            List<Card> playableMultipleCard = new List<Card>();

            CurrentHand.ForEach(card => hand.Add(card));


            hand.ForEach(card =>
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (((FractionCard)card).Fraction.isMultiple(((FractionCard)ListOrderedStacks.getStack(i).Peek()).Fraction))
                    {
                        playableMultipleCard.Add(card);
                    }
                }
            });

            playableMultipleCard.ForEach(card =>
            {
                hand.Remove(card);
            });


            while (playableMultipleCard.Count > findMultipleCard && hand.Count > 0)
            {
                findMultipleCard = playableMultipleCard.Count;

                hand.ForEach(card =>
                {
                    for (int i = 0; i < playableMultipleCard.Count; i++)
                    {
                        if (((FractionCard)card).Fraction.isMultiple(((FractionCard)ListOrderedStacks.getStack(i).Peek()).Fraction))
                        {
                            playableMultipleCard.Add(card);
                        }
                    }
                });

                for (int i = findMultipleCard; i < playableMultipleCard.Count; i++)
                {
                    hand.Remove(playableMultipleCard[i]);
                }
            }

            playableMultipleCard.ForEach(card => playableCard.Add(card));

            bool played = false;
            hand.ForEach(card =>
            {
                for (int i = 0; i < ListOrderedStacks.Size; i++)
                {
                    if (i < (ListOrderedStacks.Size * 0.5))
                    {
                        if (card.Value > ListOrderedStacks.getStack(i).Peek().Value)
                        {
                            playableCard.Add(card);
                            played = true;
                            break;
                        }
                    }
                    else if (card.Value < ListOrderedStacks.getStack(i).Peek().Value)
                    {
                        playableCard.Add(card);
                        played = true;
                        break;
                    }
                }

                if (!played)
                {
                    for (int i = 0; i < playableMultipleCard.Count; i++)
                    {
                        if (card.Value > playableMultipleCard[i].Value)
                        {
                            playableCard.Add(card);
                            played = true;
                            break;
                        }
                    }
                }

                played = false;
            });
        }
    }
}
