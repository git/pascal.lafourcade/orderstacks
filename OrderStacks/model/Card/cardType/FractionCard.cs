﻿using System;

namespace OrderStacks.model.card.cardType
{
    public class FractionCard : Card
    {

        /**
         * Type de carte
         */
        public static readonly string CARD_FRACTION = "FractionCard";

        /**
         * Fraction que représente la carte
         */
        public Fraction Fraction { get; set; }

        /**
         * <param name="value">Fraction qui est représenté sur la carte</param>
         * Constructeur
         */
        public FractionCard(Fraction value) : base(value.Result())
        {
            Fraction = value;
        }

        public override bool rapidEffect()
        {
            return false;
        }

        override public string getName()
        {
            return CARD_FRACTION;
        }

        public override string ToString()
        {
            return Fraction.ToString();
        }
    }
}
