﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OrderStacks.view
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RulesGame : ContentPage
    {
        /**
         * Constructeur de la page RulesGames page sur la quelle on arrive quand on clique sur Regles sur la page d'accueil *
         **/
        public RulesGame()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /**
         * Bouton maison qui permet de retourner sur la page d'accueil *
         **/
        private async void BackToHome(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomePage());
        }

        /**
         * Bouton jouer qui permet de se rendre sur la page GamePreparationPage *
         **/
        private async void ClickToPlay(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new GamePreparationPage());
        }

        /**
         * Bouton suivant qui permet de se rendre sur la seconde page des règles *
         **/
        private async void nextPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new SecondePageRules());
        }


    }
}