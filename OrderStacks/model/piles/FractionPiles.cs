﻿using OrderStacks.model.card.cardType;

namespace OrderStacks.model.piles
{
    public class FractionPiles : Piles
    {
        /**
         * <param name="nbPile">Nombre de pile</param>
         *
         * Constructeur
         */
        public FractionPiles(int nbPile) : base(nbPile)
        {
            for (int i = 0; i < nbPile; i++)
            {
                if (i < (nbPile * 0.5))
                {
                    ListOrderedStacks[i].Push(new FractionCard(new Fraction(0, 1, 2)));
                }
                else
                {
                    ListOrderedStacks[i].Push(new FractionCard(new Fraction(25, 1, 2)));
                }
            }
        }
    }
}
