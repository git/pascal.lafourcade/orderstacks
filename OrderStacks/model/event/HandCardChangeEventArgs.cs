﻿using System;
using OrderStacks.model.card;

namespace OrderStacks.model.@event
{
    public class HandCardChangedEventArgs : EventArgs
    {

        public Card NewCard { get; set; }
        public int Position { get; set; }

        /**
         * <param name="newCard">Nouvelle carte ajouté dans la main</param>
         * <param name="position">Position de la carte dans la main</param>
         * Constructeur
         */
        public HandCardChangedEventArgs(Card newCard, int position)
        {
            NewCard = newCard;
            Position = position;
        }
    }
}
