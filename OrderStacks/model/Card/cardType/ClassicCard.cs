﻿namespace OrderStacks.model.card.cardType
{
    public class ClassicCard : Card
    {
        /**
         * Type de carte
         */
        public static readonly string CARD_CLASSIC = "ClassicCard";

        /**
         * <param name="value">Valeur de la carte</param>
         * Constructeur
         */
        public ClassicCard(decimal value)
            : base(value)
        {
        }

        public override bool rapidEffect()
        {
            return false;
        }

        override public string getName()
        {
            return CARD_CLASSIC;
        }

    }
}