﻿namespace OrderStacks.model.manager
{
    public abstract class MultiplayerGameManager : GameManager
    {
        /**
         * <param name="parametreur">Parametreur de la partie</param>
         * Constructeur
         */
        protected MultiplayerGameManager(Parametreur parametreur) : base(parametreur)
        {
        }
    }
}
