﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OrderStacks.view
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LastPage_Rules : ContentPage
    {

        /**
         * Constructeur de la page *
         **/
        public LastPage_Rules()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /**
         * Retourne sur la page d'accueil lorsque l'on clique sur l'image de la maison *
         **/
        private async void BackToHome(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new HomePage());
        }

        /**
         * Ouvre la page GamePreparationPage lorsque l'on clique sur le bouton Jouer *
         **/
        private async void ClickToPlay(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new GamePreparationPage());
        }
        /**
         * Bouton suivant qui permet de se rendre sur la troisième page des règles *
         **/
        private async void nextPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ReglesParticuliere());
        }
        /**
         * Bouton suivant qui permet de se rendre sur la troisième page des règles *
         **/
        private async void previousPage_Rules(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}