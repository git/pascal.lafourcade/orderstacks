﻿namespace OrderStacks.model.card
{
    public abstract class Card
    {
        /**
         * Valeur de la carte
         */
        public decimal Value { get; set; }

        /**
         * <param name="value">Valeur de la carte</param>
         * Constructeur
         */
        public Card(decimal value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        /**
         * Fonction permettant de retourner le nom du type de carte
         */
        public abstract string getName();

        /**
         * Fonction permettant d'activer les effets rapides
         */
        public abstract bool rapidEffect();

    }
}
