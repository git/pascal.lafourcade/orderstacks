﻿using System.Collections.Generic;
using OrderStacks.model.card;
using OrderStacks.model.gameActions.abstractRules;
using OrderStacks.model.piles;
using OrderStacks.Resx;

namespace OrderStacks.model.gameActions.decimals
{
    public class JouerUneCarteDecimal : JouerUneCarte
    {
        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        public JouerUneCarteDecimal(Piles ListOrderedStacks) : base(ListOrderedStacks)
        {
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="currentHand">Liste de carte du joeuur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="orderedStackSelected">Pile séléctionnée</param>
         * <param name="valueCard">Valeur de la carte en train d'être joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        override public bool play(decimal valueCard, List<Card> CurrentHand, int orderedStackSelected, Player player, List<Card> CurrentCardPlayed)
        {
            foreach (Card card in CurrentHand)
            {
                if (valueCard.CompareTo(card.Value) == 0)
                {
                    if (orderedStackSelected >= 0 && orderedStackSelected < ListOrderedStacks.Size)
                    {
                        bool success;
                        if (orderedStackSelected < (ListOrderedStacks.Size * 0.5))
                        {
                            ErrorMessage = null;
                            success = Rule(card, ListOrderedStacks.getStack(orderedStackSelected), true, player, CurrentCardPlayed);
                        }
                        else
                        {
                            ErrorMessage = null;
                            success = Rule(card, ListOrderedStacks.getStack(orderedStackSelected), false, player, CurrentCardPlayed);
                        }
                        if (success)
                        {
                            CurrentHand.Remove(card);
                        }
                        return success;
                    }
                    else
                    {
                        ErrorMessage = AppResources.StrCantGetStack;
                    }
                }
            }
            ErrorMessage = AppResources.StrCardDoesntExist;
            return false;
        }

        /**
         * <param name="player">Joueur actif</param>
         * <param name="CurrentCardPlayed">Liste des cartes jouées durant le tour du joueur actif</param>
         * <param name="bottomUp">Booléen d'ascendance</param>
         * <param name="card">Carte joué</param>
         * <param name="stack">Pile joué</param>
         *
         * Fonction permettant de tenter de jouer une carte
         *
         * <returns>Booléen de carte joué</returns>
         */
        override protected bool Rule(Card card, Stack<Card> stack, bool bottomUp, Player player, List<Card> CurrentCardPlayed)
        {
            if ((bottomUp && card.Value > stack.Peek().Value) || (!bottomUp && card.Value < stack.Peek().Value))
            {
                OldCard = stack.Peek();
                player.joue(card);
                CurrentCardPlayed.Add(card);
                stack.Push(card);
                return true;
            }
            else if (card.Value.CompareTo(stack.Peek().Value - 0.1m) == 0 || card.Value.CompareTo(stack.Peek().Value + 0.1m) == 0 || card.Value.CompareTo(stack.Peek().Value - 0.01m) == 0 || card.Value.CompareTo(stack.Peek().Value + 0.01m) == 0 || card.Value.CompareTo(stack.Peek().Value - 0.001m) == 0 || card.Value.CompareTo(stack.Peek().Value + 0.001m) == 0) //  || card.Value.CompareTo(stack.Peek().Value - 10) == 0 || card.Value.CompareTo(stack.Peek().Value + 10) == 0 => creer classe abstraite JouerUneCarteFraction pour contenir isMultiple
            {
                OldCard = stack.Peek();
                player.joue(card);
                CurrentCardPlayed.Add(card);
                stack.Push(card);
                return true;
            }
            else
            {
                ErrorMessage = AppResources.StrWrongStack;
                return false;
            }
        }
    }
}
