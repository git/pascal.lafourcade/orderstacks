﻿using System;
using OrderStacks.model.card.cardType;

namespace OrderStacks.model.deck
{
    public class FractionDeck : Deck
    {

        /**
         * <param name="borneMin">Valeur minimale du deck</param>
         * <param name="borneMax">Valeur maximale du deck</param>
         * <param name="nbCard">Nombre de carte dans le deck</param>
         * Constructeur
         */
        public FractionDeck(int nbCard, decimal borneMin, decimal borneMax) : base(nbCard)
        {
            Random random = new Random();

            for (int j = 1; j < 10; j++)
            {
                for (int i = 1; i < 10; i++)
                {
                    if (deck.Count < nbCard)
                    {
                        InsertionDichotomique(0, deck.Count - 1, new FractionCard(new Fraction(i, j, 2)));
                    }
                    else
                    {
                        return;
                    }
                }
            }

            while (deck.Count < nbCard)
            {
                int numerateur = random.Next(10, 25);
                int denominateur = random.Next(10, 25);

                decimal d = (decimal)numerateur / (decimal)denominateur;
                if (d.CompareTo(borneMin) > 0 && d.CompareTo(borneMax) < 0)
                {
                    int pgcd = PGCD(numerateur, denominateur);
                    while (pgcd != 1)
                    {
                        numerateur /= pgcd;
                        denominateur /= pgcd;
                        pgcd = PGCD(numerateur, denominateur);
                    }
                    InsertionDichotomique(0, deck.Count - 1, new FractionCard(new Fraction(numerateur, denominateur, 2)));
                }
            }
        }

        /**
         * Fonction permettant de retourner le plus grand diviseur commun à deux nombres
         * <param name="a">Premier nombre</param>
         * <param name="b">Deuxième nombre</param>
         * <returns>Plus grand diviseur commun</returns>
         */
        private int PGCD(int a, int b)
        {
            int temp = a % b;
            if (temp == 0)
                return b;
            return PGCD(b, temp);
        }
    }
}
