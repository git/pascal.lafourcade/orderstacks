﻿using OrderStacks.model.card.cardType;

namespace OrderStacks.model.piles
{
    public class Piles0To1 : Piles
    {
        /**
         * <param name="nbPile">Nombre de pile</param>
         *
         * Constructeur
         */
        public Piles0To1(int nbPile) : base(nbPile)
        {
            for (int i = 0; i < nbPile; i++)
            {
                if (i < (nbPile * 0.5))
                {
                    ListOrderedStacks[i].Push(new ClassicCard(0m));
                }
                else
                {
                    ListOrderedStacks[i].Push(new ClassicCard(1m));
                }
            }
        }
    }
}
