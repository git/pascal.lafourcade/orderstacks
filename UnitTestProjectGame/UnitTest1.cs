﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheGameExtreme.model;
using TheGameExtreme.model.manager;
using TheGameExtreme.viewmodel;

namespace UnitTestProjectGame
{
    [TestClass]
    public class UnitTest1
    {
        GameManager g1 = new SoloGameManager(2, new List<String> { "clement", " baptiste" });
        
        

        [TestMethod]
        public void JoueTest()
        {
            object expectedRes = null;            
            int valueCard = 89;
            int orderedStackSelected = 3;

            g1.joue(valueCard, orderedStackSelected);            
            Assert.AreEqual(expectedRes, null);
        }

        [TestMethod]
        public void isCanPlayTest()
        {            
            bool valeur;
            bool expected1 = true;
            bool expected2 = false;
            g1.isCanPlay();
            
            
            if(g1.isCanPlay() == true)
            {
                valeur = true;
                Assert.AreEqual(expected1, valeur);
            }
            /*else
            {
                    //Exception à gérer//
            }*/            
            
            if(g1.isCanPlay() == false)
            {
                valeur = false;
                Assert.Equals(expected2, valeur);
            }
            else
            {
                    //Exception à gérer//
            }
        }

        [TestMethod]
        public void EndTurnTest()
        {
            bool valueExpected = false;
            bool valueExpected1 = true;
            bool valueBool;
            
            valueBool = g1.endTurn();
            try { 
            
                if (valueBool == false)
                {
                    Assert.AreEqual(valueExpected, valueBool);                
                }
                else
                {
                    Assert.AreEqual(valueExpected1, valueExpected1);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Nb de carte joué insufisants", e);
            }
        }

        [TestMethod]
        public void getCurentIndexPlayerTest()
        {
            int indexCurrentPlayerExpected = 0;
            int index;
            index = g1.getCurentIndexPlayer();

            Assert.AreEqual(indexCurrentPlayerExpected, index);
        }
    }
}
