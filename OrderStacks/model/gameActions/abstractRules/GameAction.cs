﻿using System;
using OrderStacks.model.piles;

namespace OrderStacks.model.gameActions.abstractRules
{
    public abstract class GameAction
    {
        protected Piles ListOrderedStacks { get; set; }
        public string ErrorMessage { get; set; } = "";

        /**
         * <param name="listOrderedStacks">Piles de jeu</param>
         * 
         * Constructeur
         */
        protected GameAction(Piles listOrderedStacks)
        {
            ListOrderedStacks = listOrderedStacks;
        }
    }
}
