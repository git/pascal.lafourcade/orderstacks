﻿using System;
using OrderStacks.model;
using OrderStacks.model.card.cardType;

namespace OrderStacks.viewmodel
{
    public class FractionCardVM : CardVM
    {

        public new FractionCard View { get; set; }

        protected Fraction fraction;
        public Fraction Fraction
        {
            get { return fraction; }
            set
            {
                fraction = value;
                View.Fraction = value;
                OnPropertyChanged("Value");
            }
        }

        /**
         * <param name="view">FractionCard représenté</param>
         * 
         * Constructeur
         */
        public FractionCardVM(FractionCard view) : base(view)
        {
            View = view;
            Fraction = view.Fraction;
        }

        /**
         * Fonction permettant de retourner le nombre maximum de chiffre présent en numérateur et/ou en dénominateur.
         * 
         * <returns>Nombre maximum de chiffre présent en numérateur et/ou dénominateur</returns>
         */
        public string getDisplayMax()
        {
            return Math.Pow(10, View.Fraction.SizeMax - 1).ToString();
        }
    }
}
